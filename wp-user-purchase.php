<?php
/**
 * Plugin Name: User Purchase
 * Description: Added method to check if a user has purchased a WooCommerce product on this site or another.
 * Version: 0.0.1
 * Author: Nathan
 * Author URI: http://greengraphics.com.au
 *
 * @package up
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


add_action(
	'rest_api_init',
	function() {
		register_rest_route(
			'user-purchase/v1',
			'/has',
			[
				[
					'methods'  => WP_REST_Server::READABLE,
					'callback' => 'user_purchase_has',
				],
			]
		);
	}
);

function user_purchase_has( \WP_REST_Request $request ) {

	$params = $request->get_query_params();

	if ( ! function_exists( 'wc_customer_bought_product' ) || ! isset( $params['product_id'] ) ) {
		return false;
	}

	$user  = get_user_by( 'id', $params['user_id'] );
	$user2 = get_user_by( 'email', $params['email'] );

	if ( ! is_array( $params['product_id'] ) ) {
		$params['product_id'] = (array) $params['product_id'];
	}

	foreach ( $params['product_id'] as $product_id ) {
		if ( wc_customer_bought_product(
			$params['email'],
			$params['user_id'],
			$product_id
		) ) {
			return true;
		}
	}

	return false;
}

add_action(
	'init',
	function() {
		if ( apply_filters( 'up_is_slave', false ) ) {
			add_filter( 'authenticate', 'up_authentication_filter', 100, 2 );
		}
	}
);

function up_authentication_filter( $auth = null, $username ) {
	if ( is_wp_error( $auth ) ) {
		return false;
	}

	$user  = get_user_by( 'login', $username );
	$roles = apply_filters( 'up_roles_to_filter', [ 'subscriber', 'customer' ] );
	if ( empty( array_intersect( $user->roles, $roles ) ) ) {
		return $auth;
	}

	if ( $user && up_check_user_purchase( $user ) ) {
		return $user;
	}

	return new WP_Error( 'authentication_failed', '<strong>ERROR:</strong> You have not purchased the correct product to get access to this website.' );
}

function up_check_user_purchase( \WP_User $user, $product_id = false ) {

	if ( $user->ID === 0 ) {
		return false;
	}

	$url   = apply_filters( 'up_remote_url', home_url() );
	$route = untrailingslashit( $url ) . '/wp-json/user-purchase/v1/has';
	// $route = untrailingslashit( $url ) . '/wp-json/user-purchase/v1/has?XDEBUG_SESSION_START=complex';
	if ( ! $product_id ) {
		$product_id = apply_filters( 'up_product_id', false );
	}

	$request_settings = [
		'timeout'   => 180,
		'sslverify' => false,
		'body'      => [
			'route'      => $route,
			'email'      => $user->user_email,
			'user_id'    => $user->ID,
			'product_id' => $product_id,
			// 'XDEBUG_SESSION_START' => 'complex',
		],
		// 'cookies'   => [
		// 'XDEBUG_SESSION_START' => 'complex',
		// ],
		// 'headers'   => [],

	];

	$hash    = md5( serialize( $request_settings ) );
	$request = get_transient( "up_check_$hash" );
	// $request = false;

	if ( ! $request ) {
		$request = wp_remote_get( $route, $request_settings );
		if ( is_wp_error( $request ) || $request['response']['code'] != 200 ) {
			return false;
		}

		set_transient( "up_check_$hash", $request, DAY_IN_SECONDS );
	}

	$data = $request['body'];

	return filter_var( $data, FILTER_VALIDATE_BOOLEAN );
}
